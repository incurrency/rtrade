#include <Rcpp.h>
#include <cmath>
#include <algorithm>
#include  <functional>
#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// duration="daily"
// symbols=c("NSENIFTY_IND___","INFY_STK___","INCURRENCY_STK___")
// type=tolower(sapply(strsplit(symbols,"_"), "[",2))
// folder=paste(datafolder,duration,"/",type,"/",sep="")
// filenames=paste(folder,symbols,".rds",sep="")

using namespace Rcpp;
using namespace std;

// [[Rcpp::depends(BH)]]
namespace bt = boost::posix_time;

//' Extracts the name of a file from the path
//'
//' The name includes the .ext value
//' @param input string, path to file
//' @export
// [[Rcpp::export]]
String getSymbolFileName(std::string input){
  vector<string> result;
  boost::split(result, input, boost::is_any_of("/"));
  int len=result.size();
  //Rcout << result[len-1] << std::endl;
  boost::split(result, result[len-1], boost::is_any_of("."));
  return result[0];
}

//' Extracts the symbol name from the file name
//'
//' The filename is required to have the .extension at the end of the filename.
//' The part of the string prior to .extension is extracted.
//' @param  input string, name of the file
// [[Rcpp::export]]
String getSymbolName(std::string input){
  vector<string> result;
  boost::split(result, input, boost::is_any_of("_"));
  int len=result.size();
  string out;
  if(len>=5){
    out=result[0];
    for(int i=1;i<5;i++){
      out=out+"_"+result[i];
    }
  }
  //Rcout << result[len-1] << std::endl;
  return out;
}

//' Checks for existence of a file
//' @param name string, path to the file
//'
bool fileExists (String name) {
  return ( access( name.get_cstring(), F_OK ) != -1 );
}

//' sorts character vector in descending mode
//' @param x, charactervector
//' @param descending, if true then descending order, else ascending

//@export
// [[Rcpp::export]]
Rcpp::CharacterVector char_sort(Rcpp::CharacterVector x,bool descending=true) {
  Rcpp::CharacterVector res = Rcpp::clone(x);
  res.sort(descending);
  return res;
}

Rcpp::StringVector sortByIndex(StringVector source, IntegerVector index){
  StringVector out=StringVector(source.length());
  for(int i=0;i<source.length();i++){
    out[i]=source[index[i]];
  }
  return out;
}


//' Reads market data for symbols into a list
//' This is a vectorized implementation of readRDS
//' @family Static Data Functions
//' @param
//' @param filenames, character vector, a complete path to the data symbol(s). filenames
//' should be sorted so that path to a symbol are adjacent.
//' @return named list,containing the complete market data for the symbol,
//' as available in the Rdatabase.
//' @export
// [[Rcpp::export]]
List readRDS2_V(StringVector filenames,bool merge=true){
  // filenames is a vector of absolute files
  // get absolute filenames for each symbol
  StringVector symbolnames_all(filenames.length());
  for (int i=0; i<filenames.length(); i++) {
    symbolnames_all[i]=getSymbolFileName(Rcpp::as< std::string >(filenames[i]));
    // if periodiciy is persecond or permin, a symbol can have multiple contributing filenames
  }
  // symbolnames are used to name the output list
  // merge ensures that multiple filenames mapping to the same symbol have the same name in the list
  if(merge){
    for (int i=0; i<filenames.length(); i++) {
      symbolnames_all[i]=getSymbolName(Rcpp::as< std::string >(symbolnames_all[i]));
    }
  }
  //Rf_PrintValue(symbolnames_all);

  Function unique_l("unique");
  //symbolnames has the unique names from symbolnames_all
  StringVector symbolnames =unique_l(symbolnames_all);
  int out_len=symbolnames.length();
  List out(out_len);
  Function readRDS_l("readRDS");
  Function rbind_l("rbind");
  int last_unique_index=0; //holds the index of the latest non-duplicate symbol
  // in list
  int j=-1; //index of the list item being created/appended. This has to be <= out_len
  for (int i=0; i<filenames.length(); i++) {
     //Rcout<<"### Loop Outer : "<<i<<": "<<symbolnames_all[i]<<std::endl;
    if((i==0) | symbolnames_all[i].operator!=( symbolnames_all[last_unique_index])){
      j=j+1; //write to the next list index
      //Rcout<<"### Loop Inner : "<<"j : "<<j<< " ,last_unique_index : "<< last_unique_index <<std::endl;
      last_unique_index=i; //set the last unique index
      if(fileExists(filenames[i])){
        out[j]=readRDS_l(Rcpp::as< std::string >(filenames[i]));
      }else{
        out[j]=DataFrame::create();
      }

    }else{
      // another set of data for existing symbol
      if(fileExists(filenames[i])){
        //  Rcout<<"### Loop  rbind : "<<"j : "<<j<< " ,i : "<< i <<std::endl;
        // Rf_PrintValue(out[j]);
        // Rf_PrintValue(readRDS_l(Rcpp::as< std::string >(filenames[i])));
        out[j]=rbind_l(out[j],readRDS_l(Rcpp::as< std::string >(filenames[i])));
      }
      // no merge needed if file does not exist
    }
  }
  out.names()=symbolnames;
  return out;

}

